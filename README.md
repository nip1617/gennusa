# NIP 2016/2017
## Andrea Gennusa


## Instructions ##
The repository is organized as follows:

* documents -> all project documentation
* sources -> source code (please create subfolders as needed)
* other -> every other thing

The documents folder is organized in subfolders

* report -> main report document
* presentation -> presentation
* notes -> please add any notes, intermediate results
* related -> you can keep here external documents


